using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PromotionEngine
{
    public class Program
    {

        internal static ShoppingCart cart;
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new PromotionEngine());
        }

        public static void addToCart(string sku)
        {
            if (cart == null) { cart = new ShoppingCart(sku); }
            else { cart.cart.Add(sku); }
        }
        public static void addToCart(List<string> sku)
        {
            if (cart == null) { cart = new ShoppingCart(sku); }
            else { 
                foreach(string _sku in sku) cart.cart.Add(_sku); }
        }

        public static string getItemFromCart(int i)
        {
            if (cart == null || i > cart.cart.Count || i < 0) { throw new IndexOutOfRangeException("Cart index is 0 based"); }
            else return cart.cart[i];
        }

        public static void clearCart()
        {
            cart.cart = new List<string>();
        }

        public static double calculateTotal(ShoppingCart cart)
        {
            return PromotionA(cart) + PromotionB(cart) + PromotionC(cart);
         }

        //a = 50; 3 of A's for 130
        public static double PromotionA(ShoppingCart cart)
        {
            double d = 0.0;
            int a = 0;
            foreach (string s in cart.cart)
            {
                if (s == "A") {
                    a += 1;
                    if (a == 3)
                    { d += 30; a = 0; }
                    else
                    { d += 50.0; }
                }
            }
            return d;
        }

        //b = 30 ; 2 * B = 2 of B's for 45 

        public static double PromotionB(ShoppingCart cart)
        {
            double d = 0.0;
            int b = 0;
            foreach (string s in cart.cart)
            {
                if (s == "B")
                {
                    b += 1;
                    if (b == 2)
                    { d += 15; b = 0; }
                    else
                    { d += 30.0; }
                }
            }
            return d;
        }

        //C = 20 D 15
        //C & D for 30
        public static double PromotionC(ShoppingCart cart)
        {
            double t = 0.0;
            int c = 0;
            int d = 0;
            foreach (string s in cart.cart)
            {
                if (s == "C")
                {
                    if (d > 0) { t += 15; d--; }
                    else { t += 20; c++; }
                }
                if (s=="D")
                {
                    if (c > 0) { t += 10; c--; }
                    else { t += 15; d++; }
                }                
            }
            return t;
        }



    }

    public class ShoppingCart
    {
        public List<string> cart = new List<string>();
        public ShoppingCart(List<string> _cart)
        {
            cart = _cart;
        }
        public ShoppingCart(string _sku)
        {
            cart.Add(_sku);
        }

    }
}

﻿
namespace PromotionEngine
{
    partial class PromotionEngine
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonA = new System.Windows.Forms.Button();
            this.buttonB = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonTOTAL = new System.Windows.Forms.Button();
            this.buttonCLEAR = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lTotal = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lCart = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonA
            // 
            this.buttonA.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonA.Location = new System.Drawing.Point(51, 46);
            this.buttonA.Name = "buttonA";
            this.buttonA.Size = new System.Drawing.Size(100, 100);
            this.buttonA.TabIndex = 0;
            this.buttonA.Text = "A";
            this.buttonA.UseVisualStyleBackColor = true;
            this.buttonA.Click += new System.EventHandler(this.buttonA_Click);
            // 
            // buttonB
            // 
            this.buttonB.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonB.Location = new System.Drawing.Point(157, 46);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(100, 100);
            this.buttonB.TabIndex = 1;
            this.buttonB.Text = "B";
            this.buttonB.UseVisualStyleBackColor = true;
            this.buttonB.Click += new System.EventHandler(this.buttonB_Click);
            // 
            // buttonC
            // 
            this.buttonC.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonC.Location = new System.Drawing.Point(263, 46);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(100, 100);
            this.buttonC.TabIndex = 2;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonD
            // 
            this.buttonD.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.buttonD.Location = new System.Drawing.Point(369, 46);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(100, 100);
            this.buttonD.TabIndex = 3;
            this.buttonD.Text = "D";
            this.buttonD.UseVisualStyleBackColor = true;
            this.buttonD.Click += new System.EventHandler(this.buttonD_Click);
            // 
            // buttonTOTAL
            // 
            this.buttonTOTAL.BackColor = System.Drawing.Color.LightGreen;
            this.buttonTOTAL.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonTOTAL.Location = new System.Drawing.Point(51, 152);
            this.buttonTOTAL.Name = "buttonTOTAL";
            this.buttonTOTAL.Size = new System.Drawing.Size(312, 100);
            this.buttonTOTAL.TabIndex = 4;
            this.buttonTOTAL.Text = "TOTAL";
            this.buttonTOTAL.UseVisualStyleBackColor = false;
            this.buttonTOTAL.Click += new System.EventHandler(this.buttonTOTAL_Click);
            // 
            // buttonCLEAR
            // 
            this.buttonCLEAR.BackColor = System.Drawing.Color.DarkOrange;
            this.buttonCLEAR.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonCLEAR.Location = new System.Drawing.Point(369, 152);
            this.buttonCLEAR.Name = "buttonCLEAR";
            this.buttonCLEAR.Size = new System.Drawing.Size(100, 100);
            this.buttonCLEAR.TabIndex = 5;
            this.buttonCLEAR.Text = "CLEAR";
            this.buttonCLEAR.UseVisualStyleBackColor = false;
            this.buttonCLEAR.Click += new System.EventHandler(this.buttonCLEAR_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lTotal);
            this.groupBox1.Location = new System.Drawing.Point(514, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 206);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Total";
            // 
            // lTotal
            // 
            this.lTotal.AutoSize = true;
            this.lTotal.Location = new System.Drawing.Point(14, 31);
            this.lTotal.Name = "lTotal";
            this.lTotal.Size = new System.Drawing.Size(35, 15);
            this.lTotal.TabIndex = 0;
            this.lTotal.Text = "Total:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lCart);
            this.groupBox2.Location = new System.Drawing.Point(58, 282);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(656, 100);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Shopping Cart";
            // 
            // lCart
            // 
            this.lCart.AutoSize = true;
            this.lCart.Location = new System.Drawing.Point(13, 35);
            this.lCart.Name = "lCart";
            this.lCart.Size = new System.Drawing.Size(32, 15);
            this.lCart.TabIndex = 0;
            this.lCart.Text = "Cart:";
            // 
            // PromotionEngine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 435);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCLEAR);
            this.Controls.Add(this.buttonTOTAL);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonB);
            this.Controls.Add(this.buttonA);
            this.Name = "PromotionEngine";
            this.Text = "PromotionEngine";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonA;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonTOTAL;
        private System.Windows.Forms.Button buttonCLEAR;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lTotal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lCart;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PromotionEngine
{
    public partial class PromotionEngine : Form
    {
        public PromotionEngine()
        {
            InitializeComponent();
        }

        private void buttonA_Click(object sender, EventArgs e)
        {
            Program.addToCart("A");
            lCart.Text = "Cart: "+ string.Join(",", Program.cart.cart);
        }

        private void buttonB_Click(object sender, EventArgs e)
        {
            Program.addToCart("B");
            lCart.Text = "Cart: " + string.Join(",", Program.cart.cart);
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            Program.addToCart("C");
            lCart.Text = "Cart: " + string.Join(",", Program.cart.cart);
        }

        private void buttonD_Click(object sender, EventArgs e)
        {
            Program.addToCart("D");
            lCart.Text = "Cart: " + string.Join(",", Program.cart.cart);

        }

        private void buttonTOTAL_Click(object sender, EventArgs e)
        {
            lTotal.Text = "Total: " + Program.calculateTotal(Program.cart);
        }

        private void buttonCLEAR_Click(object sender, EventArgs e)
        {
            Program.clearCart();
            lCart.Text = "Cart: ";
            lTotal.Text = "Total: ";
        }
    }
}

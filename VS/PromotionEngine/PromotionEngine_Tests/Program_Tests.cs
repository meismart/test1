﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit;
using PromotionEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using static PromotionEngine.Program;
using static PromotionEngine.ShoppingCart;

namespace PromotionEngine.Tests
{
    [TestFixture()]
    public class Program_Tests
    {
        static IEnumerable<List<string>> TestCarts_AddNew()
        {
            yield return new List<string> { "A" };
        }


        //A	  50
        //(3 A's for 130)
        private static readonly object[] Case_ScenarioA =
   {
        new object[] {new List<string> {"A"}, 50.0},   
        new object[] {new List<string> {"A","A"},100.0}, 
        new object[] {new List<string> {"A","A","A"},130.0}, 
        new object[] {new List<string> {"A","A", "A", "A" },180.0}, 
        new object[] {new List<string> {"A","A", "A", "A", "A" },230.0},
        new object[] {new List<string> {"A","A", "A", "A", "A", "A" },260.0}, 
        new object[] {new List<string> {"B" },0.0}, 
        new object[] {new List<string> {"B", "B" },0.0} ,
        new object[] {new List<string> {"C" },0.0}, 
        new object[] {new List<string> {"D" },0.0}, 
        new object[] {new List<string> {"C", "D" },0.0} 
    };


        //B   30
        //2 B's for 45
        private static readonly object[] Case_ScenarioB =
   {
        new object[] {new List<string> {"B"}, 30.0},   
        new object[] {new List<string> {"B","B"},45.0}, 
        new object[] {new List<string> {"B","B","B"},75.0}, 
        new object[] {new List<string> {"B","B", "B", "B" },90.0}, 
        new object[] {new List<string> {"A" },0.0}, 
        new object[] {new List<string> {"A", "A" },0.0} ,
        new object[] {new List<string> {"C" },0.0}, 
        new object[] {new List<string> {"D" },0.0}, 
        new object[] {new List<string> {"C", "D" },0.0} 
    };

        //C = 20
        //D = 15
        //C & D for 30
        private static readonly object[] Case_ScenarioCD =
   {
        new object[] {new List<string> {"C"}, 20.0},   
        new object[] {new List<string> {"D"}, 15.0},   
        new object[] {new List<string> {"C","D"},30.0}, 
        new object[] {new List<string> {"D","C"},30.0}, 
        new object[] {new List<string> {"D","D","C"},45.0}, 
        new object[] {new List<string> {"C","C", "D", "D" },60.0},
        new object[] {new List<string> {"A" },0.0}, 
        new object[] {new List<string> {"A", "D" },15.0} ,
        new object[] {new List<string> {"A", "C" },20.0}, 
        new object[] {new List<string> {"A", "B" },0.0}
    };

        private static readonly object[] Test_Scenarios =
{
        new object[] {new List<string> {"A","B","C"}, 100.0},   //Scenario 1
        new object[] {new List<string> {"A","A", "A", "A", "A", "B", "B", "B", "B", "B", "C" }, 370.0},   //Scenario 2
        new object[] {new List<string> { "A", "A", "A", "B", "B", "B", "B", "B", "C", "D" }, 280.0},   //Scenario 3
    };



        [Test()]
        public void Create_a_Cart()
        {
            ShoppingCart newCart = new ShoppingCart("A");
            Assert.IsInstanceOf(typeof(ShoppingCart), newCart);
        }


        [TestCaseSource(nameof(TestCarts_AddNew))]
        public void Add_SKU_To_Cart(List<string> _cart)
        {
            Program.addToCart(_cart);
            for (int i = 0; i < _cart.Count; i++)
            {
                Assert.AreEqual(Program.getItemFromCart(i), _cart[i]);
            }
        }

        [TestCaseSource(nameof(Case_ScenarioA))]
        public void Test_ScenarioA(List<string> _cart,double Expected_Total)
        {
            ShoppingCart cart = new ShoppingCart(_cart);

            double d = Program.PromotionA(cart);
                Assert.AreEqual(Expected_Total, d);            
        }

        [TestCaseSource(nameof(Case_ScenarioB))]
        public void Test_ScenarioB(List<string> _cart, double Expected_Total)
        {
            ShoppingCart cart = new ShoppingCart(_cart);

            double d = Program.PromotionB(cart);
            Assert.AreEqual(Expected_Total, d);
        }

        [TestCaseSource(nameof(Case_ScenarioCD))]
        public void Test_ScenarioC(List<string> _cart, double Expected_Total)
        {
            ShoppingCart cart = new ShoppingCart(_cart);

            double d = Program.PromotionC(cart);
            Assert.AreEqual(Expected_Total, d);
        }

        [TestCaseSource(nameof(Test_Scenarios))]
        public void Test_Totals(List<string> _cart, double Expected_Total)
        {
            ShoppingCart cart = new ShoppingCart(_cart);

            double d = Program.calculateTotal(cart);
            Assert.AreEqual(Expected_Total, d);
        }

        public class test_cart
        {
            public ShoppingCart cart;
            public double value;
            public double expected_value;

            public test_cart(List<string> _cart, double _expected_value)
            {
                cart = new ShoppingCart(_cart);
                expected_value = _expected_value;
            }
        }
    }
}


